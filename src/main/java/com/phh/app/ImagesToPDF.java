package com.phh.app;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;

public class ImagesToPDF {
    public static String imagesToPDF(List<String> images, String pdfDirectory) {
        // Document document = new Document(FileUtils.getMaxImageRectangle(images));
        Document document = new Document();
        String output = pdfDirectory + File.separator + new Date().getTime() + ".pdf";
        try {
            FileOutputStream fos = new FileOutputStream(output);
            PdfWriter writer = PdfWriter.getInstance(document, fos);
            writer.open();
            document.open();
//            PdfContentByte canvas = writer.getDirectContentUnder();
            for(String _image: images){
                if(!FileUtils.isExisted(_image)){
                    System.out.println("File " + _image + " is not existed");
                    continue;
                }
                Image image = Image.getInstance(_image);
//                image.scaleAbsolute(new Rectangle(image.getWidth(), image.getHeight()));
                image.scaleToFit(PageSize.A4.getWidth(), PageSize.A4.getHeight());
                image.setAlignment(Image.MIDDLE);
                document.add(image);
                String text = "\n";
                Paragraph para = new Paragraph (text);
                document.add(para);
            }

            document.close();
            writer.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            output = "";
        }

        return output;
    }
}
