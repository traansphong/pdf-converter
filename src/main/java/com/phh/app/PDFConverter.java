package com.phh.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static spark.Spark.*;
/**
 *
 */
public class PDFConverter {

    public static void main( String[] args ) throws Exception{

        ConfigProperties configProperties = new ConfigProperties();
        String outPutPath = configProperties.getOutPutPath();
        //msDoc2PDF
        get("/" + Constants.MS_DOC_TO_PDF + "/:src", (request, response) -> {
            String src = request.params(":src");
            System.out.println(src);
            if(!FileUtils.isExisted(src)){
                System.out.println("File " + src + " is not existed");
                return null;
            }
//            String targetDir = request.params(":targetDir");
            String targetDir = outPutPath;
            targetDir = targetDir.endsWith(File.separator) ? targetDir.substring(0, targetDir.length() -1): targetDir;
            String doc2PDFFile = MSOffice2PDF.convertToPDF(src, targetDir);
            System.out.println(doc2PDFFile);
            return doc2PDFFile;
        });
        //doc2PDF
        get("/" + Constants.DOC_TO_PDF + "/:src", (request, response) -> {
            String src = request.params(":src");
            System.out.println(src);
            if(!FileUtils.isExisted(src)){
                System.out.println("File " + src + " is not existed");
                return null;
            }
            //String targetDir = request.params(":targetDir");
            String targetDir = outPutPath;
            targetDir = targetDir.endsWith(File.separator) ? targetDir.substring(0, targetDir.length() -1): targetDir;
            String doc2PDFFile = OpenOffice2PDF.convert2PDF(configProperties.getOpenOfficePath(), src, targetDir);
            System.out.println(doc2PDFFile);
            return doc2PDFFile;
        });
        //libre2PDF
        get("/" + Constants.LIBRE_TO_PDF + "/:src", (request, response) -> {
            String src = request.params(":src");
            System.out.println(src);
            if(!FileUtils.isExisted(src)){
                System.out.println("File " + src + " is not existed");
                return null;
            }
            //String targetDir = request.params(":targetDir");
            String targetDir = outPutPath;
            targetDir = targetDir.endsWith(File.separator) ? targetDir.substring(0, targetDir.length() -1): targetDir;
            String doc2PDFFile = LibreOffice2PDF.convert2PDF(configProperties.getLibrePath(), src, targetDir);
            System.out.println(doc2PDFFile);
            return doc2PDFFile;
        });
        //images2PDF
        get("/" + Constants.IMAGES_TO_PDF + "/:src", (request, response) -> {
            String src = request.params(":src");
            System.out.println(src);
            //String targetDir = request.params(":targetDir");
            String targetDir = outPutPath;
            targetDir = targetDir.endsWith(File.separator) ? targetDir.substring(0, targetDir.length() -1): targetDir;
            List<String> images = Arrays.asList(src.split(Constants.COMMA));
            String images2PDFFile = ImagesToPDF.imagesToPDF(images, targetDir);
            System.out.println(images2PDFFile);
            return images2PDFFile;
        });

//        if(args != null && args.length >= 2){
//            String command = args[0];
//            String data = args[1];
//            String outPutPath = args.length == 3 ? args[2]: configProperties.getOutPutPath();
//            outPutPath = outPutPath.endsWith(File.separator) ? outPutPath.substring(0, outPutPath.length() -1): outPutPath;
//            if(command.equals(Constants.DOC_TO_PDF)){
//                String doc2PDFFile = OpenOffice2PDF.convert2PDF(configProperties.getOpenOfficePath(), data, outPutPath);
//                System.out.println(doc2PDFFile);
//            }else if(command.equals(Constants.LIBRE_TO_PDF)){
//                String doc2PDFFile = LibreOffice2PDF.convert2PDF(configProperties.getLibrePath(), data, outPutPath);
//                System.out.println(doc2PDFFile);
//            }else if(command.equals(Constants.IMAGES_TO_PDF)){
//                List<String> images = Arrays.asList(data.split(Constants.COMMA));
//                String images2PDFFile = ImagesToPDF.imagesToPDF(images, outPutPath);
//                System.out.println(images2PDFFile);
//            }else if(command.equals(Constants.MS_DOC_TO_PDF)){
//                String doc2PDFFile = MSOffice2PDF.convertToPDF(data, outPutPath);
//                System.out.println(doc2PDFFile);
//            }else {
//                System.out.println("please input format as: command data");
//                System.out.println("Example");
//                System.out.println("images2PDF `image1Path,imagesPath....` `outputPath`");
//                System.out.println("doc2PDF `docPath` `outputPath`");
//                System.out.println("msDoc2PDF `docPath` `outputPath`");
//            }
//        }else {
//            System.out.println("please input format as: command data");
//            System.out.println("Example");
//            System.out.println("images2PDF `image1Path,imagesPath....` `outputPath`");
//            System.out.println("doc2PDF `docPath` `outputPath`");
//            System.out.println("msDoc2PDF `docPath` `outputPath`");
//        }
    }

}
