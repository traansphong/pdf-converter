package com.phh.app;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Properties;

public class ConfigProperties extends Properties {

    public ConfigProperties() throws URISyntaxException, IOException {
        super();
        this.load(FileUtils.getFileFromResource("application.properties"));
        this.openOfficePath = (String)get(Constants.OFFICE_PATH);
        this.outPutPath = (String)get(Constants.OUTPUT_PATH);
        this.librePath = (String)get(Constants.LIBRE_PATH);
    }
    private String openOfficePath;
    private String outPutPath;
    private String librePath;

    public String getOpenOfficePath() {
        return openOfficePath;
    }

    public void setOpenOfficePath(String openOfficePath) {
        this.openOfficePath = openOfficePath;
    }

    public String getOutPutPath() {
        return outPutPath;
    }

    public void setOutPutPath(String outPutPath) {
        this.outPutPath = outPutPath;
    }

    public String getLibrePath() {
        return librePath;
    }

    public void setLibrePath(String librePath) {
        this.librePath = librePath;
    }
}
