package com.phh.app;

import java.io.File;
import java.util.Date;

import com.sun.star.beans.PropertyValue;
import com.sun.star.comp.helper.BootstrapException;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XDesktop;
import com.sun.star.frame.XStorable;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;

import ooo.connector.BootstrapSocketConnector;

public class OpenOffice2PDF {
    public static String convert2PDF(String openOfficePath, String docFilePath, String pdfDirectory) throws Exception, BootstrapException{
        XComponentContext xContext = BootstrapSocketConnector.bootstrap(openOfficePath);
        XMultiComponentFactory xMCF = xContext.getServiceManager();

        Object oDesktop = xMCF.createInstanceWithContext("com.sun.star.frame.Desktop", xContext);

        XDesktop xDesktop = (XDesktop) UnoRuntime.queryInterface(XDesktop.class, oDesktop);

        // Load the Document

        if (!new File(docFilePath).canRead()) {
            throw new RuntimeException("Cannot load template:" + new File(docFilePath));
        }

        XComponentLoader xCompLoader = (XComponentLoader) UnoRuntime
                .queryInterface(com.sun.star.frame.XComponentLoader.class, xDesktop);

        String sUrl = "file:///" + docFilePath;

        PropertyValue[] propertyValues = new PropertyValue[0];

        propertyValues = new PropertyValue[1];
        propertyValues[0] = new PropertyValue();
        propertyValues[0].Name = "Hidden";


        propertyValues[0].Value = new Boolean(true);

        XComponent xComp = xCompLoader.loadComponentFromURL(sUrl, "_blank", 0, propertyValues);

        // save as a PDF
        XStorable xStorable = (XStorable) UnoRuntime.queryInterface(XStorable.class, xComp);

        propertyValues = new PropertyValue[2];
        // Setting the flag for overwriting
        propertyValues[0] = new PropertyValue();
        propertyValues[0].Name = "Overwrite";
        propertyValues[0].Value = new Boolean(true);
        // Setting the filter name
        propertyValues[1] = new PropertyValue();
        propertyValues[1].Name = "FilterName";
        propertyValues[1].Value = "writer_pdf_Export";

        // Appending the favoured extension to the origin document name
        String pdfFilePath = pdfDirectory + "/" + new Date().getTime() + ".pdf"; // Name of pdf file to be output
        xStorable.storeToURL("file:///" + pdfFilePath, propertyValues);

        // shutdown
        xDesktop.terminate();

        return pdfFilePath;
    }
    public static void main(String[] args) throws Exception, BootstrapException {
        System.out.println("Stating conversion!!!");
        // Initialise
        String oooExeFolder = "C:\\Program Files (x86)\\OpenOffice 4\\program"; //Provide path on which OpenOffice is installed
        XComponentContext xContext = BootstrapSocketConnector.bootstrap(oooExeFolder);
        XMultiComponentFactory xMCF = xContext.getServiceManager();

        Object oDesktop = xMCF.createInstanceWithContext("com.sun.star.frame.Desktop", xContext);

        XDesktop xDesktop = (XDesktop) UnoRuntime.queryInterface(XDesktop.class, oDesktop);

        // Load the Document
        String workingDir = "d:/logs/"; //Provide directory path of docx file to be converted
        String myTemplate = workingDir + "HOP DONG CHO VAY TIEU DUNG.docx"; // Name of docx file to be converted

        if (!new File(myTemplate).canRead()) {
            throw new RuntimeException("Cannot load template:" + new File(myTemplate));
        }

        XComponentLoader xCompLoader = (XComponentLoader) UnoRuntime
                .queryInterface(com.sun.star.frame.XComponentLoader.class, xDesktop);

        String sUrl = "file:///" + myTemplate;

        PropertyValue[] propertyValues = new PropertyValue[0];

        propertyValues = new PropertyValue[1];
        propertyValues[0] = new PropertyValue();
        propertyValues[0].Name = "Hidden";


        propertyValues[0].Value = new Boolean(true);

        XComponent xComp = xCompLoader.loadComponentFromURL(sUrl, "_blank", 0, propertyValues);

        // save as a PDF
        XStorable xStorable = (XStorable) UnoRuntime.queryInterface(XStorable.class, xComp);

        propertyValues = new PropertyValue[2];
        // Setting the flag for overwriting
        propertyValues[0] = new PropertyValue();
        propertyValues[0].Name = "Overwrite";
        propertyValues[0].Value = new Boolean(true);
        // Setting the filter name
        propertyValues[1] = new PropertyValue();
        propertyValues[1].Name = "FilterName";
        propertyValues[1].Value = "writer_pdf_Export";

        // Appending the favoured extension to the origin document name
        String myResult = workingDir + "letterOutput.pdf"; // Name of pdf file to be output
        xStorable.storeToURL("file:///" + myResult, propertyValues);

        System.out.println("Saved " + myResult);

        // shutdown
        xDesktop.terminate();
    }
}
