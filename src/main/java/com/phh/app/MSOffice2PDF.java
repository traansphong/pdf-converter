package com.phh.app;

import com.documents4j.api.DocumentType;
import com.documents4j.api.IConverter;
import com.documents4j.job.LocalConverter;

import java.io.*;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class MSOffice2PDF {
    public static String convertToPDF(String docPath, String pdfDirectory) throws InterruptedException, java.util.concurrent.ExecutionException, IOException {
        ByteArrayOutputStream bo = new ByteArrayOutputStream();

        InputStream in = new BufferedInputStream(new FileInputStream(docPath));
        IConverter converter = LocalConverter.builder()
                .baseFolder(new File(pdfDirectory))
                .workerPool(20, 25, 2, TimeUnit.SECONDS)
                .processTimeout(5, TimeUnit.SECONDS)
                .build();

        String outputFile = pdfDirectory + File.separator + new Date().getTime() + ".pdf";
//        Future<Boolean> conversion = converter
//                .convert(in).as(DocumentType.MS_WORD)
//                .to(bo).as(DocumentType.PDF)
////                .prioritizeWith(1000) // optional
//                .schedule();
//        conversion.get();
        converter
                .convert(in).as(DocumentType.MS_WORD)
                .to(bo).as(DocumentType.PDF)
//                .prioritizeWith(1000) // optional
                .execute();
        try (OutputStream outputStream = new FileOutputStream(outputFile)) {
            bo.writeTo(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
            outputFile = "";
        }
        in.close();
        bo.close();
        converter.shutDown();

        return outputFile;
    }
}
