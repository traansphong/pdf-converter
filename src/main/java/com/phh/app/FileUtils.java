package com.phh.app;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

public class FileUtils {
    public static InputStream getFileFromResource(String fileName) throws URISyntaxException {

        ClassLoader classLoader = FileUtils.class.getClassLoader();
        InputStream resource = classLoader.getResourceAsStream(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file not found! " + fileName);
        } else {
            return resource;
        }

    }

    public static boolean isExisted(String filePath){
        return new File(filePath).exists();
    }

    public static Rectangle getMaxImageRectangle(List<String> images){
        float width = PageSize.A4.getWidth();
        float height = PageSize.A4.getHeight();
        try{
            for(String _image: images){
                if(!FileUtils.isExisted(_image)){
                    System.out.println("File " + _image + " is not existed");
                    continue;
                }
                Image image = Image.getInstance(_image);
                if(image.getWidth() > width){
                    width = image.getWidth();
                }
                if(image.getHeight() > height){
                    height = image.getHeight();
                }
            }
        }catch (IOException| BadElementException ex){
            System.out.println(ex.getMessage());
        }

        return new Rectangle(width + 50, height);
    }

}
