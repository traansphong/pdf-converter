package com.phh.app;

import java.io.File;
import java.util.Date;

import com.sun.star.beans.PropertyValue;
import com.sun.star.comp.helper.BootstrapException;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XDesktop;
import com.sun.star.frame.XStorable;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;

import ooo.connector.BootstrapSocketConnector;

public class LibreOffice2PDF {
    public static String convert2PDF(String openOfficePath, String docFilePath, String pdfDirectory) throws Exception, BootstrapException, InterruptedException {
        XComponentContext xContext = BootstrapSocketConnector.bootstrap(openOfficePath);

        XMultiComponentFactory xMCF = xContext.getServiceManager();

        Object oDesktop = xMCF.createInstanceWithContext("com.sun.star.frame.Desktop", xContext);

        XDesktop xDesktop = UnoRuntime.queryInterface(XDesktop.class, oDesktop);

        XComponentLoader xCompLoader = UnoRuntime.queryInterface(XComponentLoader.class, xDesktop);

        XComponent xComp = xCompLoader.loadComponentFromURL(
                "file:///" + docFilePath, "_blank", 0, new PropertyValue[0]);

        XStorable xStorable = UnoRuntime.queryInterface(XStorable.class, xComp);
        String pdfFilePath = pdfDirectory + "/" + new Date().getTime() + ".pdf"; // Name of pdf file to be output
        PropertyValue[] propertyValue = new PropertyValue[2];
        propertyValue[0] = new PropertyValue();
        propertyValue[0].Name = "Overwrite";
        propertyValue[0].Value = Boolean.TRUE;
        propertyValue[1] = new PropertyValue();
        propertyValue[1].Name = "FilterName";
        propertyValue[1].Value = "writer_pdf_Export";
        xStorable.storeToURL("file:///" + pdfFilePath, propertyValue);

        Thread.sleep(1000L);
        xDesktop.terminate();

        return pdfFilePath;
    }
    public static void main(String[] args) throws Exception, BootstrapException, InterruptedException {
        System.out.println("Stating conversion!!!");
        // Load the Document
        String workingDir = "d:/logs/"; //Provide directory path of docx file to be converted
        String myTemplate = workingDir + "hopdong.docx"; // Name of docx file to be converted
        String librePath = "C:/Program Files/LibreOffice/program";
        String doc2PDFFile = LibreOffice2PDF.convert2PDF(librePath, myTemplate, workingDir);
        System.out.println(doc2PDFFile);
    }
}
