package com.phh.app;

public class Constants {
    public static final String IMAGES_TO_PDF = "images2PDF";
    public static final String DOC_TO_PDF = "doc2PDF";
    public static final String LIBRE_TO_PDF = "libre2PDF";
    public static final String MS_DOC_TO_PDF = "msDoc2PDF";
    public static final String OFFICE_PATH = "path.openoffice";
    public static final String LIBRE_PATH = "path.libreoffice";
    public static final String OUTPUT_PATH = "path.output";
    public static final String COMMA = ",";
}
